const fs = require('fs');
const _ = require('../helpers')

function getArray(filename) {
  return fs.readFileSync(filename)
    .toString()
    .split(/\n/)
    .map(line => line.split(';')
      .map(v => parseFloat(v)));
}

function getBinary(n) {
  const a = (n >>> 0).toString(2);
  const b = "0000";
  return (b+a).slice(-b.length)
    .split('')
    .map(i => parseInt(i, 10));
};


function normalizarEntradas(file) {

  const array = getArray(file);
  const maxArray = getArray('./data/original/todos.csv');

  const max = maxArray
    .reduce((acc, curr) => {
      acc = acc.map((v, k) => {
        return v > curr[k] ? v : curr[k];
      });
      return acc;
    }, Array(12).fill(0));

  const arrayNormalized = array
    .map(line => line
      .reduce((acc, v, i, arr) => {
        const isLast = i === (arr.length - 1)
        if (isLast) {
          acc.target = getBinary(v);
        } else {
          acc.input.push(_.id(v/max[i]));
        }
        return acc;
      }, { input: [], target: [] })
    );

  return arrayNormalized;
}

module.exports = normalizarEntradas;