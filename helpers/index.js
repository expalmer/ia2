const normalize = n => (Number(parseFloat(Number(n).toString().split("e+")).toFixed(6)));

const id = n => normalize(n);

const media = function(n) {
  return id(0.5 * Math.pow(n, 2));
};

const mediaFinal = function(arr) {
  const total = arr.reduce((acc, x) => normalize(acc + x.media), 0);
  return id(total / arr.length);
};

const binarySum = arr => arr.reduce((acc, x, i) => { acc[i] = x ? acc[i] : 0; return acc; }, [8,4,2,1]).reduce((a, b) => a + b);

module.exports = {
  mediaFinal,
  media,
  id,
  binarySum
};