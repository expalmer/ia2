const _ = require('../helpers');

function NeuronioSaida(w) {
  this.y = 0;
  this.v = 0;
  this.setW(w[0], w[1], w[2], w[3], w[4], w[5], w[6], w[7]);
}

NeuronioSaida.prototype.setW = function(w0, w1, w2, w3, w4, w5, w6, w7) {
  this.w0 = _.id(w0);
  this.w1 = _.id(w1);
  this.w2 = _.id(w2);
  this.w3 = _.id(w3);
  this.w4 = _.id(w4);
  this.w5 = _.id(w5);
  this.w6 = _.id(w6);
  this.w7 = _.id(w7);
}

NeuronioSaida.prototype.derivada = function() {
  const y = this.y;
  return _.id(y *  (1 - y));
}

NeuronioSaida.prototype.calcula = function(x1, x2, x3, x4, x5, x6, x7) {
  this.v = _.id(this.w0 + (x1 * this.w1) + (x2 * this.w2) + (x3 * this.w3) + (x4 * this.w4) + (x5 * this.w5) + (x6 * this.w6) + (x7 * this.w7));
  this.y = _.id(1/(1 + Math.pow(Math.E, -this.v)));
}

module.exports = NeuronioSaida;