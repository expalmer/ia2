# Inteligência Artificial II - 2017
## Alunos: Diogo Machado, Palmer Oliveira
## Professor: Tales Viegas

Separamos 400 registros para o treino e generalização.

- __200__ do vinho RED
- __200__ do vinho WHITE
- __60__ do vinho RED + __60__ do vinho WHITE para **TREINAMENTO**
- __140__ do vinho RED + __140__ do vinho WHITE para **GENERALIZAÇÃO**


Embora com poucos registros, nosso sistema demora muito tempo para rodar o treino.


## Requisitos do Sistema

- NodeJS v7.4.0

## Treino

Na pasta `data/treino.csv` está o arquivo com `280` registros para o treino.

Para o treino execute o comando:

`node --stack-size=65500 index.js treinamento treinamento.csv`

Esse ``--stack-size=65500`` é necessário para aumentar o tamanho da stack, senão dá pau.

## Generalização

Para a normalização execute o comando:

`node index.js generalizacao generalizacao.csv`










